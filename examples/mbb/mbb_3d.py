from pyparalesto.pylsm import PyInput, PyLevelSetModule
from pyparalesto.pyopt import PyOptimizerModule
from fenics import *
from fenics_adjoint import *
import numpy as np
from mpi4py import MPI
import os

# Before running this script, make sure to run the following command:
# export OMP_NUM_THREADS=1

# MPI setup
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# FEniCS log level
set_log_level(30)

##################################################
# Subdomains
##################################################

class RectangularCuboid(SubDomain):
    def __init__(self, coord, tol):
        SubDomain.__init__(self)
        self.coord = coord
        self.tol = tol
    def inside(self, x, on_boundary):
        for i in range(len(x)):
            if not near(x[i], self.coord[i], self.tol[i]):
                return False
        return True

class RectangularSurface(RectangularCuboid):
    def __init__(self, coord, tol):
        RectangularCuboid.__init__(self, coord, tol)
    def inside(self, x, on_boundary):
        in_region = RectangularCuboid.inside(self, x, on_boundary)
        return on_boundary and in_region

##################################################
# Settings
##################################################

# Dimensions
lx, ly, lz = 1.0, 0.5, 0.5
nelx, nely, nelz = 40, 20, 20

# Material properties
E, nu = 1.0, 0.3

# SIMP
rho_min, penal = 1e-6, 1.0

# Print number of elements
if rank == 0:
    print("Number of elements: %d" % (nelx * nely * nelz), flush=True)

##################################################
# Initialize FEA
##################################################

# Lamé parameters
lmbda = E * nu / ((1.0 + nu) * (1.0 - 2.0 * nu))
mu = E / (2.0 * (1.0 + nu))

# Mesh
domain = BoxMesh.create([Point(0, 0, 0),
                         Point(lx, ly, lz)],
                         [nelx, nely, nelz],
                         CellType.Type.hexahedron)
domain = Mesh(domain)

# Function spaces
V = VectorFunctionSpace(domain, "CG", 1)
u, v = TrialFunction(V), TestFunction(V)
u_sol = Function(V)

# Strain and stress
def epsilon(u):
    return sym(grad(u))
def sigma(eps):
    return lmbda * tr(eps) * Identity(domain.topology().dim()) + 2.0 * mu * eps

# Density space
D = FunctionSpace(domain, "DG", 0)

# Left clamp
subdomain = RectangularSurface([0.0, 0.5 * ly, 0.5 * lz], [1e-10, ly, lz])
bc = DirichletBC(V, Constant((0., 0., 0.)), subdomain)
bcs = [bc]

# Right roller
subdomain = RectangularSurface([lx, 0.5 * ly, 0.5 * lz], [1e-10, ly, lz])
bc = DirichletBC(V.sub(0), Constant((0.)), subdomain)
bcs.append(bc)

# External load
subdomain = RectangularSurface([lx, 0.5 * ly, lz], [0.2 * lx, ly, 1e-10])
boundaries = MeshFunction("size_t", domain, domain.topology().dim() - 1, 0)
subdomain.mark(boundaries, 1)
ds = Measure('ds', domain=domain, subdomain_data=boundaries)
T = Constant((0., 0., 1.0))

##################################################
# Initialize mapping
##################################################

# Get local ranges and global size of array
Dx, Dy, Dz = lx / nelx, ly / nely, lz / nelz
Delta_div = np.array([1 / Dx, 1 / Dy, 1 / Dz])
numbering_multiplier = np.array([[1], [nelx], [nelx * nely]])

# Info for parallel communication
imap = D.dofmap()
imap_bs = imap.block_size()
imap_local_range = imap.ownership_range()
imap_size_local = imap_local_range[1] - imap_local_range[0]
local_range = np.asarray(imap_local_range, dtype=np.int32) * imap_bs
num_of_dofs_current = np.arange(*local_range)
indices_gathered = comm.gather(num_of_dofs_current, root=0)

# Communicate ranges and local data
ranges = comm.gather(local_range, root=0)

# Communicate local dof coordinates
x = D.tabulate_dof_coordinates()[:imap_size_local]
x = np.vstack(x) - 0.5 * np.array([Dx, Dy, Dz])

native_numbering = np.squeeze(np.rint(np.dot((x * Delta_div), numbering_multiplier)).astype(int))
all_nn = comm.gather(native_numbering, root=0) # gather all native numberings to one process

if rank == 0:
    stacked_all_nn = np.hstack(all_nn)
    sorted_indices = np.argsort(stacked_all_nn)

##################################################
# Initialize LSM
##################################################

# Level set initialization
if rank == 0:
    
    # Parameters
    map_flag = 1 # 0 for least squares, 1 for discrete adjoint
    perturbation = 0.15
    move_limit = 0.1

    # Initialize the input object
    pyinit = PyInput(nelx, nely, nelz, map_flag, perturbation)

    # Add a fixed region where load is applied
    pyinit.add_nondesign_domain_cuboid(nelx, 0.5 * nely, nelz,
                                       0.2 * nelx, nely, 5)
    # Initialize level set object
    pylsm = PyLevelSetModule(pyinit)

##################################################
# Initialize optimizer
##################################################

if rank == 0:
    
    # Constraints
    total_volume = float(nelx*nely*nelz)
    max_cons_vals = np.array([0.3 * total_volume])

    # Optimizer
    num_cons = max_cons_vals.size
    curr_cons_vals = np.zeros(num_cons, dtype=np.double)
    opt_algo = 0 # 0 for Newton-Raphson, 1 for MMA, 2 for Simplex
    pyopt = PyOptimizerModule(num_cons, max_cons_vals, opt_algo)

# Iterations
max_iter = 100
n_iter = 0

##################################################
# Initialize IO
##################################################

# IO initialization
if rank == 0:
    
    # Paths
    output_path = os.getcwd() + "/results_mbb_3d"

    # Clear results folder
    os.system("rm -rf " + output_path)
    os.system("mkdir " + output_path)
    os.system("mkdir " + output_path + "/level_set")

    # Create data file
    data_file = open(output_path + "/data_file.txt", "w", 1)

##################################################
# Main loop
##################################################

# Print header
if rank == 0:
    print("Iteration - Objective - Volume", flush=True)
    data_file.write("Iteration - Objective - Volume\n")

# Start optimization
comm.barrier()
while n_iter < max_iter:

    # Level-set discretization
    if rank == 0:
        
        # Compute volume fractions
        densLSM = pylsm.calculate_element_densities(False)
        volume = pylsm.get_volume()
        
        # Pass from LSM to FEniCS
        distribution = {}
        for i in range(size):
            name = 'densities_process_' + str(i)
            dummy = densLSM[stacked_all_nn[indices_gathered[i]]]
            distribution[name] = dummy
        locals().update(distribution)
    else:
        distribution = None

    # Broadcast the distribution to the different processors
    distribution = comm.bcast(distribution, root=0)
    name_local = 'densities_process_' + str(rank)
    distribution_local = np.squeeze(distribution[name_local])
    
    # Create density field
    d = Function(D) # define this inside the loop (matteo)
    d.vector()[:] = rho_min + (1.0 - rho_min) * distribution_local**penal

    # Variational forms
    L = inner(T, v) * ds(1)
    a = d * inner(sigma(epsilon(u)), epsilon(v)) * Measure("dx")
    
    # Solve PDE
    solver_options = {'linear_solver':'mumps'} #, 'preconditioner': 'ilu'}
    solve(a == L, u_sol, bcs=bcs, solver_parameters=solver_options)
    
    # Objective function (Compliance)
    J = assemble(action(L, u_sol))
    
    # Sensitivity analysis (dolfin-adjoint)
    control = Control(d)
    dJdrho = compute_gradient(J, control)
    
    # SIMP sensitivity
    if penal > 1.0:
        dJdrho.vector()[:] *= penal * (1.0 - rho_min) * distribution_local**(penal - 1.0)

    # Gather sensitivites
    dJdrho_data = comm.gather(dJdrho.vector()[:], root=0)

    # Level set optimization
    if rank == 0:
        
        # Order sensitivities
        dJdrho_data = np.hstack(dJdrho_data)
        dJdrho_single = dJdrho_data[sorted_indices]

        # Map the sensitivities
        dJ_bpt = pylsm.map_sensitivities(dJdrho_single, False)
        dg_bpt = pylsm.map_volume_sensitivities()
    
        # Print iteration
        print("%4d %12.4e %8.4f" % (n_iter, J, volume / total_volume), flush=True)
        data_file.write("%4d %12.4e %8.4f\n" % (n_iter, J, volume / total_volume))
        
        # Save level set
        pylsm.write_stl(n_iter, output_path + "/level_set", "sol_")
        
        # Get limits
        limits = pylsm.get_limits(move_limit)
        pyopt.set_limits(limits)
        curr_cons_vals[0] = volume
        
        # Solve the optimization problem
        velocities = pyopt.solve(dJ_bpt, dg_bpt, curr_cons_vals, False)
        
        # Update level set
        pylsm.update(velocities, move_limit, False)

    # Clear tape
    set_working_tape(Tape())
    
    # Update iteration
    n_iter += 1
    comm.barrier()