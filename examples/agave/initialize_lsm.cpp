//
// Copyright 2022 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "initialize_lsm.h"

#include <memory>

namespace para_lsm {

InitializeLsm::InitializeLsm () {
  // Mesh size
  nelx = 160;
  nely = 160;
  nelz = 160;

  // Create initial holes (cube shape) in design domain
  static std::vector<para_lsm::Cuboid> holesVector;
  for (int i = 10; i < nelx; i = i + 20) {
    for (int j = 10; j <= nely; j = j + 20) {
      para_lsm::Cuboid hole1 (i, j, nelz,       // Cubeoid center
                              4, 4, nelz / 2);  // half of the width of the cubeoid
      holesVector.push_back (hole1);
      //        initialVoids.push_back (std::make_shared<Cuboid> (hole1));
    }
  }

  for (int i = 0; i < holesVector.size (); i++) {
    initialVoids.push_back (std::make_shared<Cuboid> (holesVector[i]));
  }

  // Fix a cuboid (square region at the base of design domain)
  static para_lsm::Cuboid base (0.5 * nelx, 0.5 * nely, 0,       // Cuboid center
                                0.05 * nelx, 0.05 * nely, 3.0);  // half widths of the cuboid
  fixedBlobs.push_back (std::make_shared<Cuboid> (base));

  // // Use discrete adjoint sensitivity calculation
  // map_flag = 1 ;

  // // Set perturbation size for sensitivity calculation
  // perturbation = 0.15 ;
}

}  // namespace para_lsm