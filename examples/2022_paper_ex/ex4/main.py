from fenics import *
from fenics_adjoint import *
import numpy as np
import sklearn.metrics.pairwise as sp
from ufl import nabla_div
import nlopt

from pyparalesto.pyfea.solver import LinElasticPde

################################################################################
# Set up PDE for linear elasticity
################################################################################
# Define parameters
nelx    = 40
nely    = 10
nelz    = 10
lx      = 40.0
ly      = 10.0
lz      = 10.0
E       = 1
v       = 0.3
rho_min = 1e-9
dx = lx/float(nelx)
dy = ly/float(nely)
dz = lz/float(nelz)
penal = 4 # SIMP penalization parameter

# Create linear elasticity solver
pde = LinElasticPde(nelx, nely, nelz, lx, ly, lz, E, v, rho_min, penal)

# Add pinned boundary condition
pde.add_clamped_bc(0.0, 0.0, 0.0, # x, y, z coordinate of center
    1*dx, ly, 0.1*dz, # half-width in each direction
    0.0, 0.0, 0.0) # x, y, z prescribed displacement

# Add pinned boundary condition
pde.add_clamped_bc(lx, 0.0, 0.0, # x, y, z coordinate of center
    1*dx, ly, 0.1*dz, # half-width in each direction
    0.0, 0.0, 0.0) # z prescribed displacement

# Apply the body load
pde.apply_body_load(0.5*lx, 0.5*ly, lz, # x, y, z coordinate of center
    1.1*dx, ly, 1.1*dz, # half-width in each direction
    0.0, 0.0, -3.0) # Fx, Fy, Fz [N]

# Define the linear form L(v) of the variational problem
pde.define_linear_form()


################################################################################
# Set up filtering (SIMP)
################################################################################
# Define parameters
rmin = 1.5

# Prepare distance matrices for filter
midpoint = [cell.midpoint().array()[:] for cell in cells(pde.mesh)]
distance_mat = rmin - sp.euclidean_distances(midpoint, midpoint)
distance_mat[distance_mat < 0] = 0
distance_sum = distance_mat.sum(1)


################################################################################
# Set up optimization  
################################################################################
# Define parameters
volfrac = 0.4

# Initialize nlopt design variable (density)
x = volfrac*np.ones(nelx*nely*nelz)

# Define objective function and sensitivity computation for nlopt
def objfunc(x, grad):
	if grad.size > 0:
		# Set up stiffness matrix and solve linear problem
		pde.define_bilinear_form(x)
		pde.solve()
		
		# Objective function
		J = assemble(action(pde.L, pde.u_sol))
		
		# Compute sensitivity using dolfin adjoint
		control = Control(pde.density)
		dJ_drho = compute_gradient(J, control)
		
		# Filter sensitivities
		dJ_drho.vector()[:] = np.divide(distance_mat @ np.multiply(x, 
			dJ_drho.vector()[:]), np.multiply(x, distance_sum))
		
		# Put the computed sensitivity into the nlopt grad[]
		grad[:] =  dJ_drho.vector()[:]
		File('simply_supported_3D.pvd') << pde.density
	
	f = float(J)
	print('objective =', f)
	return f

# Define constraint function and sensitivity computation for nlopt
def Constraint1(x, grad):
	nelem = nelx*nely*nelz
	if grad.size > 0:
		dg = np.ones(nelem)
		grad[:] = dg
	print('constraint =', sum(x)/(nelem))
	return sum(x) - volfrac*(nelem)


################################################################################
# Optimization loop
################################################################################
l1,l2 = 0.01, 1.0
maxeval = 100
opt = nlopt.opt(nlopt.LD_MMA, nelx*nely*nelz)
opt.get_algorithm()
opt.set_min_objective(objfunc)
opt.set_lower_bounds(l1)
opt.set_upper_bounds(l2)
opt.add_inequality_constraint(Constraint1, 1e-8)
opt.set_maxeval(maxeval)

x = opt.optimize(x)


################################################################################
# Print results 
################################################################################
# Save density to xdmf
xdmf = XDMFFile("output/simply_supported.xdmf")
xdmf.write(pde.density)

# Save solution to file in VTK format
File('simply_supported_3D.pvd') << pde.density
