//
// Copyright 2022 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "utils.h"

namespace paralesto {

std::vector<double> LsmToPetscDensity (std::vector<double> lsm_rho,
                                       std::vector<int> lsm_to_petsc_map) {
  int nelem = lsm_rho.size ();
  std::vector<double> petsc_rho (nelem, 0.0);

  for (int i = 0; i < nelem; i++) {
    petsc_rho[lsm_to_petsc_map[i]] = std::max (lsm_rho[i], 0.0);
  }

  return petsc_rho;
}


std::vector<double> PetscToLsmSensitivity (std::vector<double> petsc_sens,
                                           std::vector<int> lsm_petsc_map) {
  int nelem = petsc_sens.size ();
  std::vector<double> lsm_sens (nelem, 0.0);

  for (int i = 0; i < nelem; i++) {
    lsm_sens[i] = petsc_sens[lsm_petsc_map[i]];
  }

  return lsm_sens;
}

}  // namespace paralesto