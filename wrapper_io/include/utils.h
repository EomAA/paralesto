//
// Copyright 2022 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef UTILS_H
#define UTILS_H

#include <vector>

/*! \file utils.h
    \brief A file that contains functions for interfacing between modules
*/

namespace paralesto {

//! Dummy function for mapping the level set grid densities to the PETSc mesh
/*! Note that this dummy function assumes that the level set grid and PETSc mesh
    are the same size.

    \param lsm_rho
      Vector of densities using level set grid ordering

    \param lsm_petsc_map
      Map between level set grid and PETSc map

    \return
      Vector of densities using PETSc mesh ordering
*/
std::vector<double> LsmToPetscDensity (std::vector<double> lsm_rho, std::vector<int> lsm_petsc_map);

//! Dummy function for mapping the PETSc sensitivities to the level set grid
/*! Note that this dummy function assumes that the level set grid and PETSc mesh
    are the same size.

    \param petsc_sens
      Vector of densities using level set grid ordering

    \param lsm_petsc_map
      Map between level set grid and PETSc map

    \return
      Vector of sensitivies using level set grid ordering
*/
std::vector<double> PetscToLsmSensitivity (std::vector<double> petsc_sens,
                                           std::vector<int> lsm_petsc_map);

}  // namespace paralesto

#endif