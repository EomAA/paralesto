//
// Copyright 2021 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef WRAPPERIO_H
#define WRAPPERIO_H

#include "level_set_wrapper.h"
#include "physics_wrapper.h"

/*! \file wrapper_io.h
    \brief A file that contains the class for writing output from LevelSetWrapper
    and PhysicsWrapper objects.
*/

namespace paralesto {

class WrapperIO {
 public:
  //! Constructor
  /*! \param lsm_wrap_
        Reference to the level set wrapper

      \param phys_wrap_
        Reference to the physics wrapper
  */
  WrapperIO (para_lsm::LevelSetWrapper &lsm_wrap_, para_fea::PhysicsWrapper &phys_wrap_);

  //! Print iteration information
  /*! Prints the compliance, thermal compliance, and volume constraint values
      for the current iteration.

      \param curr_iter
        Current iteration value

      \todo TODO(Carolina): implement a generic version of this function that
      works for any objective and constraint function combinations
  */
  void PrintMultiPhysIter (int curr_iter);

  //! Print iteration information
  /*! Prints the compliance, thermal compliance, and volume constraint values
      for the current iteration.

      \param curr_iter
        Current iteration value

      \todo TODO(Carolina): implement a generic version of this function that
      works for any objective and constraint function combinations
  */
  void PrintPoissonIter (int curr_iter);

  //! Print iteration information
  /*! Prints the compliance and volume constraint values for the current iteration.
      
      \param curr_iter
        Current iteration value
  */
  void PrintLinIter (int curr_iter);

  //! Write stl file of the level set boundary for the current iteration
  /*! \param curr_iter
        Current iteration value

      \param file_name
        Name of the file. Default is opt.

      \param file_path
        Name of the directory for the file to be written to. Default is
        current directory.
   */

  void WriteStl (int curr_iter, std::string file_name = "opt", std::string file_path = "");

  //! Write a vector of boundary point sensitivites to a text file.
  /*! Writes the boundary point coordinates and sensitivities to a text file.

      \param boundary_sens
        Vector of boundary point sensitivities to be printed

      \param curr_iter
        Current iteration value

      \param file_name
        Name of the file. Default is bsens.

      \param file_path
        Name of the directory for the file to be written to. Default is
        current directory.
  */
  void WriteBoundarySensTxt (std::vector<double> boundary_sens, int curr_iter,
                             std::string file_name = "bsens", std::string file_path = "");

 private:
  para_lsm::LevelSetWrapper &lsm_wrap;  //!< Reference to the level set wrapper
  para_fea::PhysicsWrapper &phys_wrap;  //!< Reference to the physics wrapper
};

}  // namespace paralesto

#endif
