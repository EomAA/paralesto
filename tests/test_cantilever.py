#
# Copyright 2022 H Alicia Kim
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import pytest

from fenics import *
from fenics_adjoint import *
import numpy as np
from ufl import nabla_div

from pyparalesto.pylsm import PyInput, PyLevelSetModule
from pyparalesto.pyopt import PyOptimizerModule
from pyparalesto.pyfea.solver import LinElasticPde

def test_lin_pde():
    ############################################################################
    # Set up PDE for linear elasticity
    ############################################################################
    # Define parameters
    nelx    = 30
    nely    = 15
    nelz    = 15
    lx      = float(nelx)
    ly      = float(nely)
    lz      = float(nelz)
    E       = 1.04
    v       = 0.3
    rho_min = 1e-5

    # Create linear elasticity solver
    pde = LinElasticPde(nelx, nely, nelz, lx, ly, lz, E, v, rho_min)

    # Add boundary condition
    pde.add_clamped_bc(0.0, 0.0, 0.0, # x, y, z coordinate of center
        1e-4, float(nely), float(nelz), # half-width in each direction
        0.0, 0.0, 0.0) # x, y, z prescribed displacement

    # Apply the body load
    pde.apply_body_load(float(nelx), nely/2.0, 0.0, # x, y, z coordinate of center
        1.0, 2.0, 2.0, # half-width in each direction
        0.0, 0.0, -1.0) # x, y, z magnitudes

    # Define the linear form L(v) of the variational problem
    pde.define_linear_form()


    ############################################################################
    # Set up level set module
    ############################################################################
    # Initialize input object with number of elements
    init = PyInput(nelx, nely, nelz)

    # Add a void region
    init.add_initial_void_cuboid(nelx/4.0, nely, nelz/3.0,
                                 nelz/10.0, nely, nelz/10.0)
    init.add_initial_void_cuboid(2*nelx/4.0, nely, nelz/3.0,
                                 nelz/10.0, nely, nelz/10.0)
    init.add_initial_void_cuboid(3*nelx/4.0, nely, nelz/3.0,
                                 nelz/10.0, nely, nelz/10.0)
    init.add_initial_void_cuboid(nelx/4.0, nely, 2*nelz/3.0,
                                 nelz/10.0, nely, nelz/10.0)
    init.add_initial_void_cuboid(2*nelx/4.0, nely, 2*nelz/3.0,
                                 nelz/10.0, nely, nelz/10.0)
    init.add_initial_void_cuboid(3*nelx/4.0, nely, 2*nelz/3.0,
                                 nelz/10.0, nely, nelz/10.0)

    # Create a level set module from input object
    lsm = PyLevelSetModule(init)


    ############################################################################
    # Set up optimization module
    ############################################################################
    # Create an optimization module
    num_cons      = 1
    total_volume  = nelx*nely*nelz
    max_cons_vals = np.array([0.4*total_volume])
    opt_algo      = 2
    opt = PyOptimizerModule(num_cons, max_cons_vals, opt_algo)


    ############################################################################
    # Define necessary parameters and data structures for optimization loop
    ############################################################################
    # Definite parameters
    curr_cons_vals = np.zeros(num_cons, dtype=np.double)
    max_iter       = 101
    count_iter     = 0
    move_limit     = 0.1
    is_print       = False


    ############################################################################
    # Optimization loop
    ############################################################################
    # Load expected objective and volume history
    obj_check = np.loadtxt("tests/obj_history.txt", dtype=np.double)
    vol_check = np.loadtxt("tests/vol_history.txt", dtype=np.double)
    while count_iter < max_iter:
        # Get the element densities
        densities = lsm.calculate_element_densities(is_print)

        # Solve the pde
        pde.define_bilinear_form(densities) # a(u,v) of the variational problem
        pde.solve() # Solve pde
        print("Finished pde solve")

        # Calculate the partial derivative wrt element densities
        J = assemble(action(pde.L, pde.u_sol)) # define compliance
        control = Control(pde.density) # define density as the design variable
        dJ_drho = compute_gradient(J, control)
        print("Finished sensitivity calculation")

        # Check objective and volume constraint
        obj = float(J)
        vol = sum(pde.density.vector()[:])/total_volume
        assert abs(obj_check[count_iter]-obj) < 1e-6
        assert abs(vol_check[count_iter]-vol) < 1e-6

        # Map element partials to boundary points
        df_bpt = lsm.map_sensitivities(dJ_drho.vector()[:], is_print)
        dg_bpt = lsm.map_volume_sensitivities()

        # Solve the suboptimization problem
        curr_cons_vals[0] = lsm.get_volume()
        if count_iter > 50:
            move_limit = 0.1
        limits = lsm.get_limits(move_limit)
        opt.set_limits(limits)
        velocities = opt.solve(df_bpt, dg_bpt, curr_cons_vals, is_print)

        # Update the topology advecting the level set
        lsm.update(velocities, move_limit, is_print)

        count_iter += 1