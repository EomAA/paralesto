//
// Copyright 2021 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "physics_wrapper.h"

#include <algorithm>
#include <fstream>
#include <limits>
#include <memory>
#include <vector>

#include "linear_elasticity.h"
#include "petsc.h"
#include "poisson.h"
#include "topopt.h"
#include "topopt1d.h"
#include "wrapper.h"

namespace para_fea {

PhysicsWrapper ::PhysicsWrapper (InitializeOpt &phys_init) {
  nelem = phys_init.nelx * phys_init.nely * phys_init.nelz;
  nnode = (phys_init.nelx + 1) * (phys_init.nely + 1) * (phys_init.nelz + 1);
  MPI_Comm_rank (PETSC_COMM_WORLD, &rank);

  // Create linear elasticity objects for pointers
  if (phys_init.is_lin_e_phys) {
    opt_ptr = std::make_unique<TopOpt> (phys_init);
    phys_lin_ptr = std::make_unique<LinearElasticity> (opt_ptr->da_nodes, phys_init);
  }

  // Create poisson objects for pointers
  if (phys_init.is_poisson_phys) {
    opt_poisson_ptr = std::make_unique<TopOpt1D> (phys_init);
    phys_poisson_ptr = std::make_unique<Poisson> (opt_poisson_ptr->da_nodes, phys_init);
  }

  wrapper_ptr = std::make_unique<Wrapper> ();

  // Resize vectors
  if (rank == 0) {
    petsc_volfrac.resize (nelem, 0.2);
    ignore_stress.resize (nelem, 0);
  }

  // Form map between level set to PETSc volume fractions
  if (phys_init.is_lin_e_phys) {
    wrapper_ptr->GetMap (opt_ptr.get (), lsm_to_petsc_map, nelem);
  } else {
    wrapper_ptr->GetMap (opt_poisson_ptr.get (), lsm_to_petsc_map, nelem);
  }
}


PhysicsWrapper ::~PhysicsWrapper () {
  opt_ptr.release ();
  phys_lin_ptr.release ();
  opt_poisson_ptr.release ();
  phys_poisson_ptr.release ();
}


std::vector<double> PhysicsWrapper ::CalculatePartialDerivatives (std::vector<double> densities,
                                                                  bool is_print) {
  if (rank == 0 && is_print) std::cout << "Starting CalculatePartialDerivatives... " << std::endl;

  // Assign volume fractions to PETSc mesh
  AssignVolumeFractionsMultiPhys (densities);
  if (rank == 0 && is_print) std::cout << "   completed assign volume fraction" << std::endl;
  MPI_Barrier (PETSC_COMM_WORLD);

  // Calculate sensitivities
  std::vector<double> elem_sens (nelem, 0.0);
  CalculateMultiPhysicsSeparateLoads (elem_sens);
  if (rank == 0 && is_print) std::cout << "   completed sensitivity calculation" << std::endl;

  return elem_sens;
}

std::vector<double> PhysicsWrapper::CalculatePartialCompliance (std::vector<double> densities,
                                                                bool is_print) {
  if (rank == 0 && is_print) std::cout << "Starting CalculatePartialCompliance... " << std::endl;

  // Assign volume fractions to PETSc mesh
  AssignVolumeFractions (densities, opt_ptr->x);
  if (rank == 0 && is_print) std::cout << "   completed assign volume fraction" << std::endl;
  MPI_Barrier (PETSC_COMM_WORLD);

  // Calculate sensitivities (state solve included)
  std::vector<double> elem_sens (nelem, 0.0);
  phys_lin_ptr->myRTOL = 1.0e-6;
  phys_lin_ptr->ComputeSensitivities (&(opt_ptr->fx), opt_ptr->dfdx, opt_ptr->x, opt_ptr->Emin,
                                      opt_ptr->Emax);

  // Gather sensitivities and assign them to a vector
  MyGather my_gather (opt_ptr->dfdx);
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      elem_sens[i] = my_gather.sensiArray[i];
    }
  }

  if (rank == 0 && is_print) std::cout << "   completed sensitivity calculation" << std::endl;

  return elem_sens;
}

std::vector<double> PhysicsWrapper ::CalculatePartialThermalCompliance (
    std::vector<double> densities, bool is_print) {
  if (rank == 0 && is_print)
    std::cout << "Starting CalculatePartialThermalCompliance... " << std::endl;

  // Assign volume fractions to PETSc mesh
  AssignVolumeFractions (densities, opt_poisson_ptr->x);
  if (rank == 0 && is_print) std::cout << "   completed assign volume fraction" << std::endl;
  MPI_Barrier (PETSC_COMM_WORLD);

  // Calculate sensitivities (state solve included)
  std::vector<double> elem_sens (nelem, 0.0);
  phys_poisson_ptr->myRTOL = 1.0e-6;
  phys_poisson_ptr->ComputeSensitivities (&(opt_poisson_ptr->fx), opt_poisson_ptr->dfdx,
                                          opt_poisson_ptr->x, opt_poisson_ptr->Kmin,
                                          opt_poisson_ptr->Kmax);

  // Gather sensitivities and assign them to a vector
  MyGather my_gather (opt_poisson_ptr->dfdx);
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      elem_sens[i] = my_gather.sensiArray[i];
    }
  }

  if (rank == 0 && is_print) std::cout << "   completed sensitivity calculation" << std::endl;

  return elem_sens;
}


void PhysicsWrapper ::ExcludeElementsFromStress (std::vector<bool> ignore_elem) {
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      ignore_stress[lsm_to_petsc_map[i]] = ignore_elem[i];
    }
    for (int i = 0; i < nelem; i++) {
      VecSetValue (opt_ptr->ignoreElemStress, i, 1.0 * ignore_stress[i], INSERT_VALUES);
    }
  }

  VecAssemblyBegin (opt_ptr->ignoreElemStress);
  VecAssemblyEnd (opt_ptr->ignoreElemStress);
}


std::vector<double> PhysicsWrapper ::CalculatePartialStress (std::vector<double> densities,
                                                             bool is_print) {
  if (rank == 0 && is_print) std::cout << "Starting CalculatePartialStress... " << std::endl;

  // Assign volume fractions to PETSc mesh
  AssignVolumeFractions (densities, opt_ptr->x);
  if (rank == 0 && is_print) std::cout << "   completed assign volume fraction" << std::endl;
  MPI_Barrier (PETSC_COMM_WORLD);

  // Calculate sensitivities (state solve included)
  std::vector<double> elem_sens (nelem, 0.0);
  phys_lin_ptr->myRTOL = 1.0e-5;
  phys_lin_ptr->ComputeStressSensitivitiesSandy (&(opt_ptr->fx), opt_ptr->dfdx, opt_ptr->x,
                                                 opt_ptr->ignoreElemStress, opt_ptr->Emin,
                                                 opt_ptr->Emax);

  // Gather sensitivities and assign them to a vector
  MyGather my_gather (opt_ptr->dfdx);
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      int index = lsm_to_petsc_map[i];
      elem_sens[i] = my_gather.sensiArray[index];
    }
  }

  if (rank == 0 && is_print) std::cout << "   completed sensitivity calculation" << std::endl;

  return elem_sens;
}


void PhysicsWrapper ::WriteDispToTxt (int count_iter, std::string file_path,
                                      std::string file_name) {
  std::vector<int> disp_to_petsc_map (1, 0);
  petsc_map::GetMap (opt_ptr->da_nodes, disp_to_petsc_map, nnode * 3);
  MyGather gather_disp (phys_lin_ptr->GetStateField ());
  if (rank == 0) {
    typedef std::numeric_limits<double> double_limit;
    std::ofstream disp_file (file_path + file_name + std::to_string (count_iter) + ".txt");
    disp_file.precision (double_limit::max_digits10);
    for (int i = 0; i < nnode * 3; i++) {
      int disp_index = disp_to_petsc_map[i];
      disp_file << gather_disp.sensiArray[disp_index] << "\n";
    }
    disp_file.close ();
  }
}


void PhysicsWrapper ::WriteTempToTxt (int count_iter, std::string file_path,
                                      std::string file_name) {
  std::vector<int> temp_to_petsc_map (1, 0);
  petsc_map::GetMap (opt_ptr->da_nodes, temp_to_petsc_map, nnode);
  MyGather gather_temp (phys_poisson_ptr->GetStateField ());
  if (rank == 0) {
    typedef std::numeric_limits<double> double_limit;
    std::ofstream temp_file (file_path + file_name + std::to_string (count_iter) + ".txt");
    temp_file.precision (double_limit::max_digits10);
    for (int i = 0; i < nnode; i++) {
      int disp_index = temp_to_petsc_map[i];
      temp_file << gather_temp.sensiArray[disp_index] << "\n";
    }
    temp_file.close ();
  }
}


void PhysicsWrapper ::AssignVolumeFractions (std::vector<double> densities, Vec x_phys) {
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      VecSetValue (x_phys, i, densities[i], INSERT_VALUES);
    }
  }

  VecAssemblyBegin (x_phys);
  VecAssemblyEnd (x_phys);
}


void PhysicsWrapper ::AssignVolumeFractionsMultiPhys (std::vector<double> densities) {
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      VecSetValue (opt_ptr->x, i, densities[i], INSERT_VALUES);
      VecSetValue (opt_poisson_ptr->x, i, densities[i], INSERT_VALUES);
    }
  }

  VecAssemblyBegin (opt_ptr->x);
  VecAssemblyEnd (opt_ptr->x);
  VecAssemblyBegin (opt_poisson_ptr->x);
  VecAssemblyEnd (opt_poisson_ptr->x);
}


PetscErrorCode PhysicsWrapper ::CalculateMultiPhysicsSeparateLoads (
    std::vector<double> &elem_sens) {
  //! Error code for debugging
  PetscErrorCode ierr;

  // Compute compliance sensitivities for structural (linear elastic) load case
  phys_lin_ptr->myRTOL = 1.0e-5;
  ierr = phys_lin_ptr->ComputeSensitivities (&(opt_ptr->fx), opt_ptr->dfdx, opt_ptr->x,
                                             opt_ptr->Emin, opt_ptr->Emax);
  CHKERRQ (ierr);
  MyGather my_gather (opt_ptr->dfdx);  // gather sensitivities to zeroeth processor

  // Compute compliance sensitivities for thermal (heat conduction) load case
  phys_poisson_ptr->myRTOL = 1.0e-6;
  ierr = phys_poisson_ptr->ComputeSensitivities (&(opt_poisson_ptr->fx), opt_poisson_ptr->dfdx,
                                                 opt_poisson_ptr->x, opt_poisson_ptr->Kmin,
                                                 opt_poisson_ptr->Kmax);
  CHKERRQ (ierr);
  MyGather my_gather_th (opt_poisson_ptr->dfdx);  // gather sensitivities

  // Add sensitivities and assign them to a vector
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      double sens_val = 0.5 * my_gather.sensiArray[i] + 0.5 * my_gather_th.sensiArray[i];
      elem_sens[i] = sens_val;
    }
  }

  return ierr;
}


LinElasticWrapper ::LinElasticWrapper (InitializeOpt &phys_init) {
  MPI_Comm_rank (PETSC_COMM_WORLD, &rank);
  nelem = phys_init.nelx * phys_init.nely * phys_init.nelz;
  nnode = (phys_init.nelx + 1) * (phys_init.nely + 1) * (phys_init.nelz + 1);

  // Create objects for pointers
  opt_ptr = std::make_unique<TopOpt> (phys_init);
  phys_ptr = std::make_unique<LinearElasticity> (opt_ptr->da_nodes, phys_init);
  wrapper_ptr = std::make_unique<Wrapper> ();

  // Resize PETSc volume fraction vector
  if (rank == 0) {
    petsc_volfrac.resize (nelem, 0.2);
  }

  // Form map between level set to PETSc volume fractions
  wrapper_ptr->GetMap (opt_ptr.get (), lsm_to_petsc_map, nelem);
}


LinElasticWrapper ::~LinElasticWrapper () {
  opt_ptr.release ();
  phys_ptr.release ();
  wrapper_ptr.release ();
}


void LinElasticWrapper ::SolveState (std::vector<double> densities, bool is_print, double rel_tol) {
  if (rank == 0 && is_print) std::cout << "Starting SolveState... " << std::endl;

  // Assign volume fractions to PETSc mesh
  AssignVolumeFractions (densities);
  if (rank == 0 && is_print) std::cout << "   completed assign volume fraction" << std::endl;
  MPI_Barrier (PETSC_COMM_WORLD);

  // Solve state equation
  phys_ptr->myRTOL = rel_tol;
  phys_ptr->SolveState (opt_ptr->x, opt_ptr->Emin, opt_ptr->Emax, opt_ptr->penal);
}


// TODO: delete after testing
MyGather LinElasticWrapper ::CalculatePartialsTest (bool is_print) {
  if (rank == 0 && is_print) std::cout << "Starting CalculatePartialDerivatives... " << std::endl;

  // Compute compliance sensitivities for structural (linear elastic) load case
  phys_ptr->ComputeSensitivitiesTest (&(opt_ptr->fx), opt_ptr->dfdx, opt_ptr->x, opt_ptr->Emin,
                                      opt_ptr->Emax);
  MyGather my_gather (opt_ptr->dfdx);  // gather sensitivities to zeroeth processor

  if (rank == 0 && is_print) std::cout << "   completed sensitivity calculation" << std::endl;

  return my_gather;
}


Vec LinElasticWrapper ::GetStateField () { return phys_ptr->GetStateField (); }


Vec LinElasticWrapper ::GetStateRhs () { return phys_ptr->GetStateRHS (); }


void LinElasticWrapper ::WriteDispToTxt (int count_iter, std::string file_path,
                                         std::string file_name) {
  std::vector<int> disp_to_petsc_map (1, 0);
  petsc_map::GetMap (opt_ptr->da_nodes, disp_to_petsc_map, nnode * 3);
  MyGather gather_disp (phys_ptr->GetStateField ());
  if (rank == 0) {
    typedef std::numeric_limits<double> double_limit;
    std::ofstream disp_file (file_path + file_name + std::to_string (count_iter) + ".txt");
    disp_file.precision (double_limit::max_digits10);
    for (int i = 0; i < nnode * 3; i++) {
      int disp_index = disp_to_petsc_map[i];
      disp_file << gather_disp.sensiArray[disp_index] << "\n";
    }
    disp_file.close ();
  }
}


void LinElasticWrapper ::AssignVolumeFractions (std::vector<double> densities) {
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      petsc_volfrac[lsm_to_petsc_map[i]] = std::max (densities[i], 0.0);
    }
    for (int i = 0; i < nelem; i++) {
      VecSetValue (opt_ptr->x, i, petsc_volfrac[i], INSERT_VALUES);
    }
  }

  VecAssemblyBegin (opt_ptr->x);
  VecAssemblyEnd (opt_ptr->x);
}


PoissonWrapper ::PoissonWrapper (InitializeOpt &phys_init) {
  MPI_Comm_rank (PETSC_COMM_WORLD, &rank);
  nelem = phys_init.nelx * phys_init.nely * phys_init.nelz;
  nnode = (phys_init.nelx + 1) * (phys_init.nely + 1) * (phys_init.nelz + 1);

  // Create objects for pointers
  opt_ptr = std::make_unique<TopOpt1D> (phys_init);
  phys_ptr = std::make_unique<Poisson> (opt_ptr->da_nodes, phys_init);
  wrapper_ptr = std::make_unique<Wrapper> ();

  // Resize PETSc volume fraction vector
  if (rank == 0) {
    petsc_volfrac.resize (nelem, 0.2);
  }

  // Form map between level set to PETSc volume fractions
  wrapper_ptr->GetMap (opt_ptr.get (), lsm_to_petsc_map, nelem);
}


PoissonWrapper ::~PoissonWrapper () {
  opt_ptr.release ();
  phys_ptr.release ();
  wrapper_ptr.release ();
}


void PoissonWrapper ::SolveState (std::vector<double> densities, bool is_print, double rel_tol) {
  if (rank == 0 && is_print) std::cout << "Starting SolveState... " << std::endl;

  // Assign volume fractions to PETSc mesh
  AssignVolumeFractions (densities);
  if (rank == 0 && is_print) std::cout << "   completed assign volume fraction" << std::endl;
  MPI_Barrier (PETSC_COMM_WORLD);

  // Solve state equation
  phys_ptr->myRTOL = rel_tol;
  bool with_convection = false;
  phys_ptr->SolveState (opt_ptr->x, opt_ptr->Kmin, opt_ptr->Kmax, with_convection, opt_ptr->penal);
}


// TODO: delete after testing
MyGather PoissonWrapper ::CalculatePartialsTest (bool is_print) {
  if (rank == 0 && is_print) std::cout << "Starting CalculatePartialDerivatives... " << std::endl;

  // Compute compliance sensitivities for thermal (heat conduction) load case
  phys_ptr->ComputeSensitivities (&(opt_ptr->fx), opt_ptr->dfdx, opt_ptr->x, opt_ptr->Kmin,
                                  opt_ptr->Kmax);
  MyGather my_gather_th (opt_ptr->dfdx);  // gather sensitivities

  if (rank == 0 && is_print) std::cout << "   completed sensitivity calculation" << std::endl;

  return my_gather_th;
}


Vec PoissonWrapper ::GetStateField () { return phys_ptr->GetStateField (); }


Vec PoissonWrapper ::GetStateRhs () { return phys_ptr->GetStateRHS (); }


void PoissonWrapper ::WriteTempToTxt (int count_iter, std::string file_path,
                                      std::string file_name) {
  std::vector<int> temp_to_petsc_map (1, 0);
  petsc_map::GetMap (opt_ptr->da_nodes, temp_to_petsc_map, nnode);
  MyGather gather_temp (phys_ptr->GetStateField ());
  if (rank == 0) {
    typedef std::numeric_limits<double> double_limit;
    std::ofstream temp_file (file_path + file_name + std::to_string (count_iter) + ".txt");
    temp_file.precision (double_limit::max_digits10);
    for (int i = 0; i < nnode; i++) {
      int disp_index = temp_to_petsc_map[i];
      temp_file << gather_temp.sensiArray[disp_index] << "\n";
    }
    temp_file.close ();
  }
}


void PoissonWrapper ::AssignVolumeFractions (std::vector<double> densities) {
  if (rank == 0) {
    for (int i = 0; i < nelem; i++) {
      petsc_volfrac[lsm_to_petsc_map[i]] = std::max (densities[i], 0.0);
    }
    for (int i = 0; i < nelem; i++) {
      VecSetValue (opt_ptr->x, i, petsc_volfrac[i], INSERT_VALUES);
    }
  }

  VecAssemblyBegin (opt_ptr->x);
  VecAssemblyEnd (opt_ptr->x);
}

}  // namespace para_fea