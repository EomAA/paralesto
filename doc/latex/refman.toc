\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Todo List}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Hierarchical Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class Hierarchy}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Class List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}File List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Class Documentation}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Blob Class Reference}{9}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Detailed Description}{10}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Constructor \& Destructor Documentation}{10}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}Blob()}{10}{subsubsection.5.1.2.1}
\contentsline {subsection}{\numberline {5.1.3}Member Function Documentation}{10}{subsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.3.1}GetMinDist()}{10}{subsubsection.5.1.3.1}
\contentsline {subsubsection}{\numberline {5.1.3.2}RotatePoint()}{11}{subsubsection.5.1.3.2}
\contentsline {section}{\numberline {5.2}Boundary Class Reference}{11}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Detailed Description}{13}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Constructor \& Destructor Documentation}{13}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}Boundary()}{13}{subsubsection.5.2.2.1}
\contentsline {subsection}{\numberline {5.2.3}Member Function Documentation}{13}{subsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.3.1}DomainDistance()}{13}{subsubsection.5.2.3.1}
\contentsline {subsubsection}{\numberline {5.2.3.2}FixLocDistance()}{13}{subsubsection.5.2.3.2}
\contentsline {subsubsection}{\numberline {5.2.3.3}LinearInterp()}{14}{subsubsection.5.2.3.3}
\contentsline {subsubsection}{\numberline {5.2.3.4}MarchCubes()}{14}{subsubsection.5.2.3.4}
\contentsline {subsubsection}{\numberline {5.2.3.5}Optimize()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{14}{subsubsection.5.2.3.5}
\contentsline {subsubsection}{\numberline {5.2.3.6}Optimize()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{15}{subsubsection.5.2.3.6}
\contentsline {subsubsection}{\numberline {5.2.3.7}WritePatch()}{15}{subsubsection.5.2.3.7}
\contentsline {subsubsection}{\numberline {5.2.3.8}WriteSTL()}{15}{subsubsection.5.2.3.8}
\contentsline {section}{\numberline {5.3}Cuboid Class Reference}{15}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Detailed Description}{17}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Constructor \& Destructor Documentation}{17}{subsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.2.1}Cuboid()}{17}{subsubsection.5.3.2.1}
\contentsline {subsection}{\numberline {5.3.3}Member Function Documentation}{17}{subsection.5.3.3}
\contentsline {subsubsection}{\numberline {5.3.3.1}GetMinDist()}{17}{subsubsection.5.3.3.1}
\contentsline {subsubsection}{\numberline {5.3.3.2}RotatePoint()}{18}{subsubsection.5.3.3.2}
\contentsline {section}{\numberline {5.4}Cut\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Plane Class Reference}{18}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Detailed Description}{19}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Constructor \& Destructor Documentation}{19}{subsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.2.1}CutPlane()}{19}{subsubsection.5.4.2.1}
\contentsline {subsection}{\numberline {5.4.3}Member Function Documentation}{20}{subsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.3.1}GetMinDist()}{20}{subsubsection.5.4.3.1}
\contentsline {subsubsection}{\numberline {5.4.3.2}RotatePoint()}{20}{subsubsection.5.4.3.2}
\contentsline {section}{\numberline {5.5}Cylinder Class Reference}{21}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Detailed Description}{22}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Constructor \& Destructor Documentation}{22}{subsection.5.5.2}
\contentsline {subsubsection}{\numberline {5.5.2.1}Cylinder()}{22}{subsubsection.5.5.2.1}
\contentsline {subsection}{\numberline {5.5.3}Member Function Documentation}{22}{subsection.5.5.3}
\contentsline {subsubsection}{\numberline {5.5.3.1}GetMinDist()}{22}{subsubsection.5.5.3.1}
\contentsline {subsubsection}{\numberline {5.5.3.2}RotatePoint()}{23}{subsubsection.5.5.3.2}
\contentsline {section}{\numberline {5.6}Fixed\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Dof Class Reference}{23}{section.5.6}
\contentsline {section}{\numberline {5.7}Force Class Reference}{24}{section.5.7}
\contentsline {section}{\numberline {5.8}Grid Class Reference}{24}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}Detailed Description}{25}{subsection.5.8.1}
\contentsline {subsection}{\numberline {5.8.2}Constructor \& Destructor Documentation}{25}{subsection.5.8.2}
\contentsline {subsubsection}{\numberline {5.8.2.1}Grid()}{25}{subsubsection.5.8.2.1}
\contentsline {subsection}{\numberline {5.8.3}Member Function Documentation}{25}{subsection.5.8.3}
\contentsline {subsubsection}{\numberline {5.8.3.1}GridPtToIndex()}{25}{subsubsection.5.8.3.1}
\contentsline {section}{\numberline {5.9}Grid4\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Vector Class Reference}{26}{section.5.9}
\contentsline {subsection}{\numberline {5.9.1}Detailed Description}{26}{subsection.5.9.1}
\contentsline {subsection}{\numberline {5.9.2}Constructor \& Destructor Documentation}{26}{subsection.5.9.2}
\contentsline {subsubsection}{\numberline {5.9.2.1}Grid4Vector()}{26}{subsubsection.5.9.2.1}
\contentsline {section}{\numberline {5.10}Grid\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Vector Class Reference}{27}{section.5.10}
\contentsline {subsection}{\numberline {5.10.1}Detailed Description}{28}{subsection.5.10.1}
\contentsline {subsection}{\numberline {5.10.2}Constructor \& Destructor Documentation}{28}{subsection.5.10.2}
\contentsline {subsubsection}{\numberline {5.10.2.1}GridVector()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{28}{subsubsection.5.10.2.1}
\contentsline {subsubsection}{\numberline {5.10.2.2}GridVector()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{28}{subsubsection.5.10.2.2}
\contentsline {section}{\numberline {5.11}Heat\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Load Class Reference}{28}{section.5.11}
\contentsline {section}{\numberline {5.12}H\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}J\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}W\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}O\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stencil Class Reference}{29}{section.5.12}
\contentsline {subsection}{\numberline {5.12.1}Detailed Description}{30}{subsection.5.12.1}
\contentsline {subsection}{\numberline {5.12.2}Constructor \& Destructor Documentation}{30}{subsection.5.12.2}
\contentsline {subsubsection}{\numberline {5.12.2.1}HJWENOStencil()}{30}{subsubsection.5.12.2.1}
\contentsline {subsection}{\numberline {5.12.3}Member Function Documentation}{31}{subsection.5.12.3}
\contentsline {subsubsection}{\numberline {5.12.3.1}CapIndices()}{31}{subsubsection.5.12.3.1}
\contentsline {subsubsection}{\numberline {5.12.3.2}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{31}{subsubsection.5.12.3.2}
\contentsline {subsubsection}{\numberline {5.12.3.3}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{32}{subsubsection.5.12.3.3}
\contentsline {subsubsection}{\numberline {5.12.3.4}GradValue()}{32}{subsubsection.5.12.3.4}
\contentsline {subsubsection}{\numberline {5.12.3.5}InterpolateGrad()}{32}{subsubsection.5.12.3.5}
\contentsline {subsubsection}{\numberline {5.12.3.6}IsInBounds()}{33}{subsubsection.5.12.3.6}
\contentsline {section}{\numberline {5.13}Initialize\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Opt Class Reference}{33}{section.5.13}
\contentsline {section}{\numberline {5.14}Level\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Set3D Class Reference}{34}{section.5.14}
\contentsline {subsection}{\numberline {5.14.1}Detailed Description}{36}{subsection.5.14.1}
\contentsline {subsection}{\numberline {5.14.2}Constructor \& Destructor Documentation}{36}{subsection.5.14.2}
\contentsline {subsubsection}{\numberline {5.14.2.1}LevelSet3D()}{36}{subsubsection.5.14.2.1}
\contentsline {subsection}{\numberline {5.14.3}Member Function Documentation}{37}{subsection.5.14.3}
\contentsline {subsubsection}{\numberline {5.14.3.1}AdjustForCFL()}{37}{subsubsection.5.14.3.1}
\contentsline {subsubsection}{\numberline {5.14.3.2}ComputeAggregationSensitivities()}{37}{subsubsection.5.14.3.2}
\contentsline {subsubsection}{\numberline {5.14.3.3}ComputeElementalVolFracs()}{38}{subsubsection.5.14.3.3}
\contentsline {subsubsection}{\numberline {5.14.3.4}DifferntiateAggVf()}{38}{subsubsection.5.14.3.4}
\contentsline {subsubsection}{\numberline {5.14.3.5}GridPtToIndex()}{39}{subsubsection.5.14.3.5}
\contentsline {subsubsection}{\numberline {5.14.3.6}IndexToGridPt()}{39}{subsubsection.5.14.3.6}
\contentsline {subsubsection}{\numberline {5.14.3.7}IsInBounds()}{39}{subsubsection.5.14.3.7}
\contentsline {subsubsection}{\numberline {5.14.3.8}SetGridDimensions()}{41}{subsubsection.5.14.3.8}
\contentsline {subsubsection}{\numberline {5.14.3.9}SmoothPhi()}{41}{subsubsection.5.14.3.9}
\contentsline {subsubsection}{\numberline {5.14.3.10}SolveEikonal()}{41}{subsubsection.5.14.3.10}
\contentsline {subsubsection}{\numberline {5.14.3.11}UpdateVelocity()}{42}{subsubsection.5.14.3.11}
\contentsline {subsubsection}{\numberline {5.14.3.12}WriteSD()}{42}{subsubsection.5.14.3.12}
\contentsline {subsection}{\numberline {5.14.4}Member Data Documentation}{42}{subsection.5.14.4}
\contentsline {subsubsection}{\numberline {5.14.4.1}volume}{42}{subsubsection.5.14.4.1}
\contentsline {section}{\numberline {5.15}Linear\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Elasticity Class Reference}{43}{section.5.15}
\contentsline {section}{\numberline {5.16}Min\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stencil Class Reference}{44}{section.5.16}
\contentsline {subsection}{\numberline {5.16.1}Detailed Description}{45}{subsection.5.16.1}
\contentsline {subsection}{\numberline {5.16.2}Constructor \& Destructor Documentation}{45}{subsection.5.16.2}
\contentsline {subsubsection}{\numberline {5.16.2.1}MinStencil()}{45}{subsubsection.5.16.2.1}
\contentsline {subsection}{\numberline {5.16.3}Member Function Documentation}{46}{subsection.5.16.3}
\contentsline {subsubsection}{\numberline {5.16.3.1}CapIndices()}{46}{subsubsection.5.16.3.1}
\contentsline {subsubsection}{\numberline {5.16.3.2}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{46}{subsubsection.5.16.3.2}
\contentsline {subsubsection}{\numberline {5.16.3.3}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{46}{subsubsection.5.16.3.3}
\contentsline {subsubsection}{\numberline {5.16.3.4}IsInBounds()}{47}{subsubsection.5.16.3.4}
\contentsline {subsubsection}{\numberline {5.16.3.5}MinValue()}{47}{subsubsection.5.16.3.5}
\contentsline {section}{\numberline {5.17}Moment\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Line\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Segment Class Reference}{47}{section.5.17}
\contentsline {subsection}{\numberline {5.17.1}Member Data Documentation}{48}{subsection.5.17.1}
\contentsline {subsubsection}{\numberline {5.17.1.1}wGauss}{48}{subsubsection.5.17.1.1}
\contentsline {subsubsection}{\numberline {5.17.1.2}xGauss}{48}{subsubsection.5.17.1.2}
\contentsline {section}{\numberline {5.18}Moment\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Polygon Class Reference}{49}{section.5.18}
\contentsline {subsection}{\numberline {5.18.1}Member Data Documentation}{50}{subsection.5.18.1}
\contentsline {subsubsection}{\numberline {5.18.1.1}xGauss}{50}{subsubsection.5.18.1.1}
\contentsline {section}{\numberline {5.19}Moment\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Quadrature Class Reference}{50}{section.5.19}
\contentsline {section}{\numberline {5.20}My\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Gather Class Reference}{50}{section.5.20}
\contentsline {section}{\numberline {5.21}My\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Optimizer Class Reference}{51}{section.5.21}
\contentsline {subsection}{\numberline {5.21.1}Detailed Description}{52}{subsection.5.21.1}
\contentsline {subsection}{\numberline {5.21.2}Constructor \& Destructor Documentation}{52}{subsection.5.21.2}
\contentsline {subsubsection}{\numberline {5.21.2.1}MyOptimizer()}{52}{subsubsection.5.21.2.1}
\contentsline {section}{\numberline {5.22}My\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Optimizer\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}::nlopt\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}cons\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}grad\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}data Struct Reference}{53}{section.5.22}
\contentsline {section}{\numberline {5.23}Poisson Class Reference}{53}{section.5.23}
\contentsline {section}{\numberline {5.24}Sensitivity Class Reference}{54}{section.5.24}
\contentsline {subsection}{\numberline {5.24.1}Detailed Description}{55}{subsection.5.24.1}
\contentsline {subsection}{\numberline {5.24.2}Constructor \& Destructor Documentation}{55}{subsection.5.24.2}
\contentsline {subsubsection}{\numberline {5.24.2.1}Sensitivity()}{55}{subsubsection.5.24.2.1}
\contentsline {section}{\numberline {5.25}Stencil Class Reference}{55}{section.5.25}
\contentsline {subsection}{\numberline {5.25.1}Detailed Description}{57}{subsection.5.25.1}
\contentsline {subsection}{\numberline {5.25.2}Constructor \& Destructor Documentation}{57}{subsection.5.25.2}
\contentsline {subsubsection}{\numberline {5.25.2.1}Stencil()}{57}{subsubsection.5.25.2.1}
\contentsline {subsection}{\numberline {5.25.3}Member Function Documentation}{58}{subsection.5.25.3}
\contentsline {subsubsection}{\numberline {5.25.3.1}CapIndices()}{58}{subsubsection.5.25.3.1}
\contentsline {subsubsection}{\numberline {5.25.3.2}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{58}{subsubsection.5.25.3.2}
\contentsline {subsubsection}{\numberline {5.25.3.3}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{59}{subsubsection.5.25.3.3}
\contentsline {subsubsection}{\numberline {5.25.3.4}IsInBounds()}{59}{subsubsection.5.25.3.4}
\contentsline {section}{\numberline {5.26}Top\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Opt Class Reference}{60}{section.5.26}
\contentsline {section}{\numberline {5.27}Top\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Opt1D Class Reference}{61}{section.5.27}
\contentsline {section}{\numberline {5.28}Triangle Struct Reference}{62}{section.5.28}
\contentsline {subsection}{\numberline {5.28.1}Detailed Description}{62}{subsection.5.28.1}
\contentsline {section}{\numberline {5.29}Vel\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Grad\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stencil Class Reference}{62}{section.5.29}
\contentsline {subsection}{\numberline {5.29.1}Detailed Description}{63}{subsection.5.29.1}
\contentsline {subsection}{\numberline {5.29.2}Constructor \& Destructor Documentation}{63}{subsection.5.29.2}
\contentsline {subsubsection}{\numberline {5.29.2.1}VelGradStencil()}{63}{subsubsection.5.29.2.1}
\contentsline {subsection}{\numberline {5.29.3}Member Function Documentation}{64}{subsection.5.29.3}
\contentsline {subsubsection}{\numberline {5.29.3.1}CapIndices()}{64}{subsubsection.5.29.3.1}
\contentsline {subsubsection}{\numberline {5.29.3.2}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [1/2]}}{64}{subsubsection.5.29.3.2}
\contentsline {subsubsection}{\numberline {5.29.3.3}GetValue()\hspace {0.1cm}{\relax \fontsize {8}{9.5}\selectfont \abovedisplayskip 6\p@ plus2\p@ minus4\p@ \abovedisplayshortskip \z@ plus\p@ \belowdisplayshortskip 3\p@ plus\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 4\p@ plus2\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 4\p@ plus2\p@ minus\p@ {\leftmargin \leftmargini \topsep 3\p@ plus\p@ minus\p@ \parsep 2\p@ plus\p@ minus\p@ \itemsep \parsep }\belowdisplayskip \abovedisplayskip \ttfamily [2/2]}}{64}{subsubsection.5.29.3.3}
\contentsline {subsubsection}{\numberline {5.29.3.4}GetVelGrad()}{65}{subsubsection.5.29.3.4}
\contentsline {subsubsection}{\numberline {5.29.3.5}IsInBounds()}{65}{subsubsection.5.29.3.5}
\contentsline {section}{\numberline {5.30}Wrapper Class Reference}{66}{section.5.30}
\contentsline {chapter}{\numberline {6}File Documentation}{67}{chapter.6}
\contentsline {section}{\numberline {6.1}lsm/include/boundary.h File Reference}{67}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Detailed Description}{67}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}lsm/include/grid\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}math.h File Reference}{67}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Detailed Description}{68}{subsection.6.2.1}
\contentsline {section}{\numberline {6.3}lsm/include/lsm\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}3d.h File Reference}{68}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Detailed Description}{69}{subsection.6.3.1}
\contentsline {section}{\numberline {6.4}lsm/include/optimize.h File Reference}{69}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Detailed Description}{69}{subsection.6.4.1}
