var searchData=
[
  ['jacobian_5ftranspose_5fvector_5fproduct_1140',['jacobian_transpose_vector_product',['../classpyparalesto_1_1pyfea_1_1solver_1_1LinElasticPde.html#ac5a1507e9554a5c6e687aec3535a0a7b',1,'pyparalesto.pyfea.solver.LinElasticPde.jacobian_transpose_vector_product()'],['../classpyparalesto_1_1pyfea_1_1solver_1_1PoissonPde.html#a485dbaa7da0bf429ba7877c736badbf4',1,'pyparalesto.pyfea.solver.PoissonPde.jacobian_transpose_vector_product()']]],
  ['jacobian_5fvector_5fproduct_1141',['jacobian_vector_product',['../classpyparalesto_1_1pyfea_1_1solver_1_1LinElasticPde.html#ac7b3e4f0622230ea39ed19582afe495b',1,'pyparalesto.pyfea.solver.LinElasticPde.jacobian_vector_product()'],['../classpyparalesto_1_1pyfea_1_1solver_1_1PoissonPde.html#af28e4c11f2af6d8a12adf71df91346af',1,'pyparalesto.pyfea.solver.PoissonPde.jacobian_vector_product()']]]
];
