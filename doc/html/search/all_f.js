var searchData=
[
  ['objfungrad_521',['objFunGrad',['../classpara__lsm_1_1MyOptimizer.html#aae8314fa0778e3184d5e2843fe112910',1,'para_lsm::MyOptimizer::objFunGrad()'],['../classpara__lsm_1_1OptimizeIpopt.html#aef111e895377c930173a9bd3001825cb',1,'para_lsm::OptimizeIpopt::objFunGrad()']]],
  ['operator_20gridvector_522',['operator GridVector',['../classpara__lsm_1_1Grid4Vector.html#a067894081615aba76f9b183f2e4d82a0',1,'para_lsm::Grid4Vector']]],
  ['operator_28_29_523',['operator()',['../classlsm__2d_1_1MersenneTwister.html#a4dedd4335fdb4ce80b306426fcfade81',1,'lsm_2d::MersenneTwister']]],
  ['operator_3d_524',['operator=',['../classpara__lsm_1_1OptimizeIpopt.html#aa8cecc53946f0ffec47e3bcc767c10a7',1,'para_lsm::OptimizeIpopt']]],
  ['opt_5falgo_525',['opt_algo',['../classpara__lsm_1_1OptimizerWrapper.html#a727970588b2a217ec39a8e7a04531af2',1,'para_lsm::OptimizerWrapper']]],
  ['opt_5fpoisson_5fptr_526',['opt_poisson_ptr',['../classpara__fea_1_1PhysicsWrapper.html#a092c856166eb38fc2e57a2e048e04efc',1,'para_fea::PhysicsWrapper']]],
  ['opt_5fptr_527',['opt_ptr',['../classpara__fea_1_1PoissonWrapper.html#a42ac5b917652760c8852f7ccf63b7128',1,'para_fea::PoissonWrapper::opt_ptr()'],['../classpara__fea_1_1LinElasticWrapper.html#a927c461be3791b8f1866bd26cd36fb56',1,'para_fea::LinElasticWrapper::opt_ptr()'],['../classpara__fea_1_1PhysicsWrapper.html#aaf1d7951a7213b768a0c014cfa51e820',1,'para_fea::PhysicsWrapper::opt_ptr()']]],
  ['opt_5fvel_528',['opt_vel',['../classpara__lsm_1_1Boundary.html#afd5a8ed7d8fd772c9ce04df1d7e8fa57',1,'para_lsm::Boundary']]],
  ['optimize_529',['Optimize',['../classpara__lsm_1_1Boundary.html#a2631c37a66e3d8ab2b191f0793829fa2',1,'para_lsm::Boundary::Optimize(std::vector&lt; double &gt; &amp;bsens, double moveLimit, double volume, double volCons, int algo=0)'],['../classpara__lsm_1_1Boundary.html#a89fb3bdc2043c1402312e730eb282fde',1,'para_lsm::Boundary::Optimize(std::vector&lt; double &gt; &amp;objSens, std::vector&lt; std::vector&lt; double &gt;&gt; &amp;conSens, std::vector&lt; double &gt; conMaxVals, std::vector&lt; double &gt; conCurVals, double moveLimit, int algo)']]],
  ['optimize_2ecpp_530',['optimize.cpp',['../optimize_8cpp.html',1,'']]],
  ['optimize_2eh_531',['optimize.h',['../optimize_8h.html',1,'']]],
  ['optimize_5fipopt_2ecpp_532',['optimize_ipopt.cpp',['../optimize__ipopt_8cpp.html',1,'']]],
  ['optimize_5fipopt_2eh_533',['optimize_ipopt.h',['../optimize__ipopt_8h.html',1,'']]],
  ['optimizeipopt_534',['OptimizeIpopt',['../classpara__lsm_1_1OptimizeIpopt.html#afb772dc31ca4985ed21eba856a6149e5',1,'para_lsm::OptimizeIpopt::OptimizeIpopt(std::vector&lt; double &gt; &amp;z, std::vector&lt; double &gt; &amp;z_lo, std::vector&lt; double &gt; &amp;z_up, std::vector&lt; double &gt; &amp;objFunGrad, std::vector&lt; std::vector&lt; double &gt;&gt; &amp;conFunGrad, std::vector&lt; double &gt; conMaxVals)'],['../classpara__lsm_1_1OptimizeIpopt.html#aeb32b54bdf6c080dcf058b68877ae2e9',1,'para_lsm::OptimizeIpopt::OptimizeIpopt(const OptimizeIpopt &amp;)'],['../classpara__lsm_1_1OptimizeIpopt.html',1,'para_lsm::OptimizeIpopt']]],
  ['optimizer_5fwrapper_2ecpp_535',['optimizer_wrapper.cpp',['../optimizer__wrapper_8cpp.html',1,'']]],
  ['optimizer_5fwrapper_2eh_536',['optimizer_wrapper.h',['../optimizer__wrapper_8h.html',1,'']]],
  ['optimizerwrapper_537',['OptimizerWrapper',['../classpara__lsm_1_1OptimizerWrapper.html#a620ba38d781c161d6112d87b99633ea8',1,'para_lsm::OptimizerWrapper::OptimizerWrapper()'],['../classpara__lsm_1_1OptimizerWrapper.html',1,'para_lsm::OptimizerWrapper']]],
  ['outofbounds_538',['outOfBounds',['../classlsm__2d_1_1FastMarchingMethod.html#a40ee24e09e1a6fac727b9d467e0debcf',1,'lsm_2d::FastMarchingMethod']]],
  ['outside_539',['OUTSIDE',['../namespacelsm__2d_1_1NodeStatus.html#a6686ef57dcba66443239bd6f0a6eccebaa64841d4efaece22c2a7d04c6762e001',1,'lsm_2d::NodeStatus::OUTSIDE()'],['../namespacelsm__2d_1_1ElementStatus.html#a5c387d366c07e07ab3e7a72656ca9de9a74511a0977cd0fcbd706eaef3cafcb63',1,'lsm_2d::ElementStatus::OUTSIDE()']]],
  ['vector3d_540',['Vector3d',['../classpara__lsm_1_1GridVector.html#a8ad4bd01234cc3b44de3b9798e18585a',1,'para_lsm::GridVector']]]
];
